﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SteeringMoveController))]
[RequireComponent(typeof(FleeSteering))]
[RequireComponent(typeof(EvadeSteering))]
[RequireComponent(typeof(NavMeshSteering))]
public class NPCController : MonoBehaviour
{
    public float safeDistance = 30;
    public float dangerDistance = 20;
    public LayerMask bulletMask = -1;

    EvadeSteering evade;
    FleeSteering flee;
    NavMeshSteering nav;
    void Awake ()
    {
        evade = GetComponent<EvadeSteering>();
        flee = GetComponent<FleeSteering>();
        nav = GetComponent<NavMeshSteering>();
    }
    
    void Start ()
    {
        flee.steeringTarget = PlayerController.player.transform;
        nav.steeringTarget = PlayerController.player.transform;
    }

    void Update ()
    {
        bool inDanger = false;
        Collider[] c = Physics.OverlapSphere(transform.position, safeDistance, bulletMask);
        for (int i = 0; i < c.Length; i++)
        {
            Vector3 bulletDiff = (transform.position - c[i].transform.position).normalized;
            Rigidbody bullet = c[i].GetComponentInParent<Rigidbody>();
            if (bullet && Vector3.Dot(bulletDiff, bullet.velocity.normalized) > 0) {

                float frac = (safeDistance - bulletDiff.magnitude) / (safeDistance - dangerDistance);

                inDanger = true;
                nav.weight = 1-frac;
                flee.weight = 0.1f;
                evade.weight = frac;
                evade.steeringTarget = c[i].transform;
                break;
            }
        }

        Vector3 playerDiff = PlayerController.player.transform.position - transform.position;
        if (playerDiff.sqrMagnitude < safeDistance * safeDistance)
        {
            float frac = (safeDistance - playerDiff.magnitude) / (safeDistance - dangerDistance);

            nav.weight = 0;
            flee.weight = inDanger ? 0 : frac;
            evade.weight = inDanger ? 1 : 0;
        }
        else if (!inDanger) 
        {
            nav.weight = 1;
            flee.weight = 0;
            evade.weight = 0;
        }
        
    }
}
