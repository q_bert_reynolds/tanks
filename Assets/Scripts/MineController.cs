﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineController : MonoBehaviour
{   
    public ParticleSystem particles;
    public enum States
    {
        Set,
        Armed,
        Explode
    }
    public States state = States.Set;

    TankController tank;
    Animator anim;
    void Awake ()
    {
        anim = GetComponent<Animator>();
    }

    public void Set(TankController tank)
    {
        this.tank = tank;
        state = States.Set;
    }

    void OnDisable()
    {
        state = States.Set;
        if (tank == null) return;
        tank.MineDisabled();
        tank = null;
    }

    void OnTriggerExit ()
    {
        if (state == States.Set)
        {
            state = States.Armed;
            anim.Play("Arm");
        }
    }

    void OnTriggerEnter (Collider c)
    {
        if (state == States.Armed)
        {
            anim.Play("Explosion");
        }

        if (state == States.Explode)
        {
            TankController tank = c.GetComponentInParent<TankController>();
            BulletController bullet = c.GetComponent<BulletController>();
            Destructible destructible = c.GetComponent<Destructible>();
            if (tank)
            {
                tank.Explode();
            }
            else if (bullet)
            {
                bullet.gameObject.SetActive(false);
                ParticleManager.Play("Explosion", c.transform.position);
            }
            else if (destructible)
            {
                destructible.PlayDestruction();
            }
        }
    }

    public void Explode ()
    {
        state = States.Explode;
        particles.Play();
        AudioManager.Play("death");
        AudioManager.Play("explosion");
    }

    public void FinishExplosion ()
    {
        state = States.Set;
        anim.Play("Set");
        gameObject.SetActive(false);
    }

}
