﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{

    public void PlayDestruction ()
    {
        ParticleManager.Play("Explosion", transform.position);
        AudioManager.Play("explosion");
        gameObject.SetActive(false);
    }
}
