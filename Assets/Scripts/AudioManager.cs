﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager audioMan;

    public AudioClip[] clips;
    public int maxSources = 10;
    AudioSource[] sfxPool;
    int currentSound = 0;

    void Awake ()
    {
        if (audioMan == null) 
        {
            audioMan = this;
            DontDestroyOnLoad(gameObject);
            SetupSFX();
        }
        else
        {
            Debug.Log("AudioManager already exists, destroying this one.");
            Destroy(gameObject);
        }
    }

    void SetupSFX ()
    {
        sfxPool = new AudioSource[maxSources];
        for (int i = 0; i < maxSources; i++)
        {
            GameObject g = new GameObject("SFX." + i);
            g.transform.SetParent(transform);
            AudioSource source = g.AddComponent<AudioSource>();
            sfxPool[i] = source;
        }
    }

    public static void Play(string clipName)
    {
        Play(clipName, Vector3.zero, 1, 1, 0, 0);
    }

    public static void PlayRandom(string clipName)
    {
        float pitch = Random.Range(0.9f, 1.1f);
        float volume = Random.Range(0.8f, 1);
        Play(clipName, Vector3.zero, pitch, volume, 0, 0);
    }
    
    public static void Play(string clipName, Vector3 pos, float pitch, float volume, float spatialBlend, float pan)
    {
        for (int i = 0; i < audioMan.clips.Length; i++)
        {
            if (clipName == audioMan.clips[i].name)
            {
                int index = audioMan.currentSound;
                AudioSource source = audioMan.sfxPool[index];
                source.transform.position = pos;
                source.pitch = pitch;
                source.volume = volume;
                source.spatialBlend = spatialBlend;
                source.panStereo = pan;
                source.clip = audioMan.clips[i];
                source.Play();
                audioMan.currentSound = (index + 1) % audioMan.maxSources;
                return;
            }
        }
    }
}
