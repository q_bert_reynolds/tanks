﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoader : MonoBehaviour
{
    public string[] blockPrefabNames;
    public string[] enemyPrefabNames;
    public TextAsset[] levels;

    public void LoadLevel (int level) 
    {
        Spawner.DisableAll();

        string levelString = levels[level].text;
        string[] rows = levelString.Split(new char[]{'\n'});
        int numRows = rows.Length;
        for (int row = 0; row < numRows; row++)
        {
            int numColumns = 22;//rows[row].Length;
            for (int column = 0; column < numColumns; column++)
            {
                char c = rows[row][column];
                Vector3 pos = new Vector3(
                    column * 4 - 2*numColumns + 3, 
                    0, 
                    2*numRows - row * 4 - 3
                );

                if (char.IsDigit(c))
                {
                    int num = (int)char.GetNumericValue(c);
                    if (num < blockPrefabNames.Length) 
                    {
                        GameObject block = Spawner.Spawn(blockPrefabNames[num], pos);                        
                    }
                }
                else if (c == 'P')
                {
                    PlayerController.player.transform.position = pos;
                    PlayerController.player.gameObject.SetActive(true);
                }
                else if (char.IsLower(c)) {
                    int num = (int)(c - 'a');
                    if (num < enemyPrefabNames.Length) 
                    {
                        GameObject enemy = Spawner.Spawn(enemyPrefabNames[num], pos);
                    }
                }
            }
        }
    }
}
