﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BulletController : MonoBehaviour
{
    public float speed = 10;
    public int maxBounces = 1;
    int bounces = 0;

    ParticleSystem trail;
    TankController tank;
    Rigidbody body;
    void Awake ()
    {
        trail = GetComponentInChildren<ParticleSystem>();
        body = GetComponent<Rigidbody>();
    }

    void OnDisable ()
    {
        if (tank == null) return;
        tank.BulletDisabled();
        tank = null;
    }

    public void Fire(Ray ray, TankController tank)
    {
        this.tank = tank;
        bounces = 0;
        transform.position = ray.origin;
        transform.forward = ray.direction;
        gameObject.SetActive(true);
        body.velocity = ray.direction * speed + tank.GetComponent<Rigidbody>().velocity * 0.1f;
        if (trail != null)
        {
            trail.Stop();
            trail.transform.SetParent(transform);
            trail.transform.localPosition = Vector3.back;
            trail.Play();
        }
    }

    void OnCollisionEnter (Collision c)
    {
        TankController tank = c.gameObject.GetComponent<TankController>();
        BulletController bullet = c.gameObject.GetComponent<BulletController>();
        if (tank || bullet)
        {
            Hit();
            if (tank) tank.Explode();
            else if (bullet) bullet.Hit();
        }
    }

    void OnCollisionExit (Collision c)
    {
        if (bounces == maxBounces)
        {
            Hit();
            return;
        }

        Vector3 dir = body.velocity.normalized;
        transform.forward = dir;
        body.velocity = dir * speed;
        bounces++;
        AudioManager.Play("bounce");
    }

    public void Hit ()
    {
        if (trail) trail.transform.SetParent(null);
        gameObject.SetActive(false);
        ParticleManager.Play("Explosion", transform.position);
        AudioManager.Play("laserHit");
    }
}
