﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    public static ParticleManager particleMan;

    public ParticleSystem[] prefabs;
    public int maxParticles = 5;
    ParticleSystem[][] particlePools;
    int[] currentParticles;

    void Awake ()
    {
        if (particleMan == null)
        {
            particleMan = this;
            DontDestroyOnLoad(gameObject);
            SetupParticles();
        }
        else
        {
            Debug.Log("ParticleManager already exists, destroying this one.");
            Destroy(gameObject);
        }
    }

    void SetupParticles ()
    {
        particlePools = new ParticleSystem[prefabs.Length][];
        currentParticles = new int[maxParticles];
        for (int i = 0; i < prefabs.Length; i++)
        {
            particlePools[i] = new ParticleSystem[maxParticles];
            for (int j = 0; j < maxParticles; j++)
            {
                ParticleSystem p = Instantiate(prefabs[i]) as ParticleSystem;
                p.transform.SetParent(transform);
                particlePools[i][j] = p;
            }
        }
    }

    public static void Play(string particleName, Vector3 pos)
    {
        for (int i = 0; i < particleMan.prefabs.Length; i++)
        {
            if (particleName == particleMan.prefabs[i].name)
            {
                int index = particleMan.currentParticles[i];
                ParticleSystem p = particleMan.particlePools[i][index];
                p.transform.position = pos;
                p.Play();
                particleMan.currentParticles[i] = (index + 1) % particleMan.maxParticles;
                return;
            }
        }
    }

    public static void DisableAll()
    {
        for (int i = 0; i < particleMan.prefabs.Length; i++)
        {
            for (int j = 0; j < particleMan.maxParticles; j++)
            {
                ParticleSystem p = particleMan.particlePools[i][j];
                p.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            }
        }
    }
}
