﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAimController : MonoBehaviour
{
    public float aimInterval = 1;
    float lastAim;

    TankController tank;
    void Awake()
    {
        tank = GetComponent<TankController>();
    }

    void Update()
    {
        Vector3 diff = PlayerController.player.transform.position - transform.position;
        float dot = Vector3.Dot(diff.normalized, tank.gunTip.forward);
        if (dot > 0.9f && tank.CanFire()) {
            Ray ray = new Ray(tank.gunTip.position, tank.gunTip.forward);
            RaycastHit hit;
            if (Physics.SphereCast(ray, 0.5f, out hit, tank.hitMask) 
                && hit.rigidbody != null
                && hit.rigidbody.GetComponent<PlayerController>()) 
            { 
                lastAim = Time.time;
                tank.SetAim(diff);
                tank.Fire();
            }
        }

        if (Time.time - lastAim > aimInterval)
        {
            lastAim = Time.time;
            Vector3 rand = Random.insideUnitSphere;
            rand.y = 0;
            tank.SetAim(rand);
        }
    }
}
