﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SweepAimController : MonoBehaviour
{
    public int maxBounces = 0;

    Vector3 bestDir;

    TankController tank;
    void Awake()
    {
        tank = GetComponent<TankController>();
        bestDir = -tank.gunTip.forward;
    }
    void Update()
    {
        float angle = Mathf.Atan2(-tank.gunTip.forward.y, tank.gunTip.forward.x);
        bestDir = transform.forward;
        int leastBounces = maxBounces+1;
        bool found = false;
        for (float a = 0; a < 260; a += 1) {
            float rad = (angle+a) * Mathf.Deg2Rad;
            Vector3 dir = new Vector3(
                Mathf.Cos(rad), 0, Mathf.Sin(rad)
            );

            Vector3 origin = transform.position + Vector3.up + dir * 5;
            Ray ray = new Ray(origin, dir);
            RaycastHit hit;
            for (int i = 0; i <= maxBounces; i++)
            {
                if (Physics.SphereCast(ray, 0.5f, out hit, tank.hitMask))
                {
                    PlayerController player = hit.collider.GetComponentInParent<PlayerController>();
                    TankController otherTank = hit.collider.GetComponentInParent<TankController>();
                    if (player != null) 
                    {
                        found = true;
                        if (i < leastBounces) {
                            bestDir = dir;
                        }
                    }
                    else if (otherTank != null)
                    {
                        break;
                    }
                    else 
                    {
                        ray.origin = hit.point + hit.normal*0.5f;
                        ray.direction = ray.direction - 2 * Vector3.Dot(ray.direction, hit.normal) * hit.normal;
                    }
                }
            }
            if (leastBounces == 0)
            {
                break;
            }

        }
        if (found) tank.SetAim(bestDir);
        
        float dot = Vector3.Dot(bestDir, tank.gunTip.forward);
        if (dot > 0.99f) tank.Fire();

    }

    void OnDrawGizmos ()
    {
        TankController tank = GetComponent<TankController>();
        Ray ray = new Ray(tank.gunTip.position, tank.gunTip.forward);
        RaycastHit hit;
        for (int i = 0; i <= maxBounces; i++)
        {
            if (Physics.Raycast(ray, out hit))
            {
                Gizmos.color = Color.black;
                Gizmos.DrawLine(ray.origin, hit.point);
                ray.origin = hit.point;
                ray.direction = ray.direction - 2 * Vector3.Dot(ray.direction, hit.normal) * hit.normal;
            }
        }
    }
}
