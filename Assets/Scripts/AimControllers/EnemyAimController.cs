﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAimController : MonoBehaviour
{
    TankController tank;
    void Awake ()
    {
        tank = GetComponent<TankController>(); 
    }

    void Update()
    {
        Vector3 diff = PlayerController.player.transform.position - transform.position;
        tank.SetAim(diff);        
        tank.Fire();
    }
}
