﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankController))]
public class PlayerController : MonoBehaviour
{
    public static PlayerController player;

    public enum ControlEnum
    {
        Direct,
        Skid
    }  
    public ControlEnum controls = ControlEnum.Direct;
    
    TankController tank;
    Camera cam;
    void Awake ()
    {
        if (player == null) player = this;
        cam = Camera.main;
        tank = GetComponent<TankController>();
    }
    
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        if (controls == ControlEnum.Direct) {
            tank.SetMove(new Vector3(x, 0, z));
        }
        else if (controls == ControlEnum.Skid)
        {
            Vector3 rot = transform.eulerAngles;
            rot.y += x * tank.turnSpeed * Time.deltaTime;
            transform.eulerAngles = rot;
            Vector3 dir = transform.forward * z;
            tank.SetMove(dir);
        }

        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            Vector3 diff = hit.point - transform.position;
            tank.SetAim(diff);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            tank.Fire();
        }
        
        if (Input.GetButtonDown("Fire2"))
        {
            tank.SetMine();
        }
    }
}
