﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager game;

    public int level = 1;
    public int lives = 3;
    public int kills = 0;
    public int enemiesLeft = 5;
    public enum GameState
    {
        PreGame,
        Count,
        Playing,
        PostGame
    }
    public GameState gameState = GameState.PreGame;

    LevelLoader levelLoader;

    [Header("PreGame")]
    public Material scrollingBG;
    public Canvas preGameCanvas;
    public Text preGameLevelText;
    public Text preGameEnemyText;
    public Text preGameLivesText;
    public float preGameDuration = 3;
    float preGameTimer;

    [Header("Game")]
    public Canvas gameCanvas;
    public Text gameLevelText;
    public Text gameEnemiesLeftText;
    public Text gameKillCountText;
    public float countDownDuration = 3;
    float countDownTimer;

    [Header("PostGame")]
    public Canvas postGameCanvas;
    public Text postGameKillCountText;

    public static bool isPlaying {
        get { return game != null && game.gameState == GameState.Playing; }
    }

    void Awake () 
    {
        if (game == null) game = this;
    }
    
    void Start () 
    {
        if (isPlaying) Play();
        else 
        {
            levelLoader = FindObjectOfType<LevelLoader>();
            ShowPreGame();
        }
    }

    void UpdateLivesText ()
    {
        string s = "✗ " + lives;
        preGameLivesText.text = s;
    }

    void UpdateLevelText ()
    {
        string s = "Mission " + level.ToString();
        preGameLevelText.text = s;
        gameLevelText.text = s;
    }

    void UpdateScoreText ()
    {
        gameKillCountText.text = kills.ToString();
        postGameKillCountText.text = kills.ToString();
        gameEnemiesLeftText.text = enemiesLeft.ToString();
    }

    void Update ()
    {
        switch (gameState)
        {
            case GameState.PreGame:
                UpdatePreGame();
                break;
            case GameState.Count:
                UpdateCount();
                break;
            case GameState.PostGame:
                UpdatePostGame();
                break;
            default:
                break;
        }
    }

    void ShowPreGame()
    {
        preGameCanvas.gameObject.SetActive(true);
        gameCanvas.gameObject.SetActive(false);
        postGameCanvas.gameObject.SetActive(false);
        Spawner.DisableAll();
        ParticleManager.DisableAll();

        Time.timeScale = 0;
        gameState = GameState.PreGame;
        preGameTimer = Time.unscaledTime;
        game.levelLoader.LoadLevel(game.level);
        TankController[] tanks = FindObjectsOfType<TankController>();
        enemiesLeft = tanks.Length - 1;

        UpdateLevelText();
        UpdateLivesText();
        UpdateScoreText();
    }

    void UpdatePreGame ()
    {
        float t = Time.unscaledTime % 1f;
        scrollingBG.mainTextureOffset = new Vector2(-t, -t);
        if (Time.unscaledTime - preGameTimer > preGameDuration && Input.GetButtonUp("Fire1")) StartCount();
    }

    void UpdatePostGame ()
    {
        if (Time.unscaledTime - preGameTimer > preGameDuration && Input.GetButtonUp("Fire1")) ShowPreGame();
    }


    void StartCount()
    {
        Time.timeScale = 1;
        preGameCanvas.gameObject.SetActive(false);
        gameCanvas.gameObject.SetActive(false);
        postGameCanvas.gameObject.SetActive(false);
        gameState = GameState.Count;
        countDownTimer = Time.unscaledTime;
    }

    void UpdateCount()
    {
        if (Time.unscaledTime - countDownTimer > countDownDuration) Play();
    }

    void Play()
    {
        Time.timeScale = 1;
        gameCanvas.gameObject.SetActive(true);
        gameState = GameState.Playing;
    }

    void ShowPostGame ()
    {
        Time.timeScale = 0;
        gameState = GameState.PostGame;
        preGameCanvas.gameObject.SetActive(false);
        gameCanvas.gameObject.SetActive(false);
        postGameCanvas.gameObject.SetActive(true);
        UpdateLevelText();
        UpdateLivesText();
        UpdateScoreText();
    }

    public static void TankKilled(TankController tank)
    {
        if (tank.GetComponent<PlayerController>())
        {
            game.ShowPreGame();
        }
        else {
            game.enemiesLeft--;
            game.kills++;
            game.UpdateScoreText();

            if (game.enemiesLeft == 0)
            {
                game.level++;
                game.ShowPostGame();
            }
        }
    }
}
