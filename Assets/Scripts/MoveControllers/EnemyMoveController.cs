﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveController : MonoBehaviour
{
    public float chaseDist = 12;
    public float fleeDist = 8;

    TankController tank;
    void Awake ()
    {
        tank = GetComponent<TankController>(); 
    }

    void Update()
    {
        Vector3 diff = PlayerController.player.transform.position - transform.position;
        float distSqr = diff.sqrMagnitude;

        if (distSqr > chaseDist * chaseDist)
        {
            tank.SetMove(diff);
        }
        else if (distSqr < fleeDist * fleeDist)
        {
            tank.SetMove(-diff);
        }
        else
        {
            tank.SetMove(Vector3.zero);
        }
        
    }

    void OnDrawGizmos ()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, chaseDist);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, fleeDist);
    }
}
