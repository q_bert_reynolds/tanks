﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringMoveController : MonoBehaviour
{
    SteeringBehaviour[] steering;

    TankController tank;
    void Awake ()
    {
        tank = GetComponent<TankController>();
        steering = GetComponentsInChildren<SteeringBehaviour>();
    }

    void Update ()
    {
        Vector3 dir = Vector3.zero;
        for (int i = 0; i < steering.Length; i++) {
            dir += steering[i].GetSteering();
        }
        tank.SetMove(dir);
    }
}
