﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehaviour : MonoBehaviour
{
    public Transform steeringTarget;
    public Vector3 steeringPosition;
    public float weight = 1;
    protected Vector3 steeringDir;

    public virtual Vector3 GetSteering ()
    {
        return steeringDir.normalized * weight;
    }
}
