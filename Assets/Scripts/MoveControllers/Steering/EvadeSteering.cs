﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvadeSteering : FleeSteering
{
    public override Vector3 GetSteering ()
    {
        if (steeringTarget == null) return base.GetSteering();
        Rigidbody body = steeringTarget.GetComponent<Rigidbody>();
        if (body == null) return base.GetSteering();

        Vector3 diff = transform.position - body.position;
        float t = diff.magnitude / body.velocity.magnitude;
        Vector3 dest = body.position + body.velocity * t;
        steeringDir = transform.position - dest;
        
        return steeringDir.normalized * weight;
    }

}
