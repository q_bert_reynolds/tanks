﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshSteering : SteeringBehaviour
{
    public bool autoCalc = true;
    public float calcInterval = 1f;
    float lastCalc;
    int pathIndex = 0;

    NavMeshPath path;
    NavMeshPath testPath;
    void Awake ()
    {
        path = new NavMeshPath();
        testPath = new NavMeshPath();
    }

    void Update()
    {
        if (autoCalc && Time.time - lastCalc > calcInterval) CalculatePath();

        if (pathIndex >= path.corners.Length) return;
        
        Vector3 target = path.corners[pathIndex];
        Vector3 diff = target - transform.position;        
        if (diff.sqrMagnitude > 4)
        {
            steeringDir = diff;
        }
        else
        {
            steeringDir = Vector3.zero;
            pathIndex++;
        }
    }

    public void CalculatePath()
    {
        lastCalc = Time.time;
        pathIndex = 0;
        if (steeringTarget != null) steeringPosition = steeringTarget.position;
        NavMesh.CalculatePath(transform.position, steeringPosition, NavMesh.AllAreas, path);
    }

    public bool CanReach (Vector3 pos)
    {
        NavMesh.CalculatePath(transform.position, pos, NavMesh.AllAreas, testPath);
        return testPath.status == NavMeshPathStatus.PathComplete;
    }

    void OnDrawGizmos ()
    {
        if (!Application.isPlaying || path == null) return;
        
        for (int i = 0; i < path.corners.Length-1; i++)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(path.corners[i], path.corners[i+1]);
        }
    }
}
