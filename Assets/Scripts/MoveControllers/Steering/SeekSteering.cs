﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekSteering : SteeringBehaviour
{
    public override Vector3 GetSteering ()
    {
        if (steeringTarget != null) steeringPosition = steeringTarget.position;
        steeringDir = steeringPosition - transform.position;
        return base.GetSteering();
    }
}
