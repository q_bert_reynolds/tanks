﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeSteering : SeekSteering
{
    public override Vector3 GetSteering ()
    {
        return -base.GetSteering();
    }
}
