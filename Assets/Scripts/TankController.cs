﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TankController : MonoBehaviour
{
    public float speed = 5;
    public float turnSpeed = 10;
    public float aimSpeed = 10;
    public float maxSpeedChange = 1;
    public float fireInterval = 0.1f;
    public LayerMask hitMask = -1;
    float lastShot;
    float lastMine;

    public Transform turret;
    public Transform gunTip;

    public string bulletName = "OneBounce";
    int maxBounces;
    public int maxBullets = 6;
    int bulletsLeft;
    public int maxMines = 1;
    int minesLeft;

    public float trackDist = 2;
    Vector3 lastTrackPosition;

    Vector3 moveDir;
    Vector3 aimDir;
    Rigidbody body;
    SkinnedMeshRenderer tankMesh;
    void Awake () 
    {
        tankMesh = GetComponentInChildren<SkinnedMeshRenderer>();
        body = GetComponent<Rigidbody>();
        lastTrackPosition = transform.position + transform.forward;
    }

    void Start ()
    {
        bulletsLeft = maxBullets;
        minesLeft = maxMines;
        GameObject bulletPrefab = Spawner.GetPrefab(bulletName);
        if (bulletPrefab) maxBounces = bulletPrefab.GetComponent<BulletController>().maxBounces;
    }

    public void SetMove(Vector3 dir)
    {
        dir.y = 0;
        moveDir = dir.normalized;
    }

    public void SetAim(Vector3 dir)
    {
        dir.y = 0;
        aimDir = dir.normalized;
    }

    public bool CanFire()
    {
        return GameManager.isPlaying && Time.time - lastShot > fireInterval;
    }

    public void Fire()
    {
        if (!CanFire()) return;
        
        if (bulletsLeft > 0) {
            GameObject g = Spawner.Spawn(bulletName, gunTip.position);
            BulletController bullet = g.GetComponent<BulletController>();
            Ray ray = new Ray(gunTip.position, gunTip.forward);
            bullet.Fire(ray, this);
            bulletsLeft--;
            AudioManager.Play("heavyLaser");
            lastShot = Time.time;
            return;
        }
    }

    public void SetMine()
    {
        if (!GameManager.isPlaying) return;

        if (Time.time - lastMine < fireInterval) return;
        
        if (minesLeft > 0)
        {
            AudioManager.Play("select");
            GameObject g = Spawner.Spawn("Mine", transform.position);
            MineController mine = g.GetComponent<MineController>();
            mine.Set(this);
            lastMine = Time.time;
            return;
        }
    }

    void UpdateTreads()
    {
        float scroll = (tankMesh.materials[1].mainTextureOffset.x + body.velocity.magnitude * Time.deltaTime) % 1f;
        tankMesh.materials[1].mainTextureOffset = Vector3.right * scroll;

        float sqrDist = (transform.position + transform.forward - lastTrackPosition).sqrMagnitude;
        if (sqrDist > trackDist * trackDist)
        {
            lastTrackPosition = transform.position + transform.forward;
            GameObject track = Spawner.Spawn("Tracks", transform.position);
            track.transform.forward = transform.forward;
        }
    }

    void Update ()
    { 
        if (aimDir.magnitude > 0.1f)
        {
            turret.forward = Vector3.Lerp(turret.forward, aimDir, aimSpeed * Time.deltaTime);
        }

        if (!GameManager.isPlaying) return;
        
        UpdateTreads();
        if (moveDir.magnitude > 0.1f) 
        {
            float dot = Vector3.Dot(transform.forward, moveDir);
            Vector3 dir = transform.forward;
            if (dot < 0) dir *= -1;
            Vector3 cross = Vector3.Cross(dir, moveDir).y * Vector3.up;
            transform.eulerAngles += cross * Mathf.Rad2Deg * turnSpeed * Time.deltaTime;
        }
        
    }

    void FixedUpdate ()
    {
        if (!GameManager.isPlaying) 
        {
            body.velocity = Vector3.zero;
            return;
        }
        Vector3 targetVelocity = moveDir * speed;
        Vector3 dV = targetVelocity - body.velocity;
        dV.y = 0;
        if (dV.sqrMagnitude > maxSpeedChange * maxSpeedChange)
        {
            dV = dV.normalized * maxSpeedChange;
        }

        body.AddForce(dV, ForceMode.VelocityChange);
    }

    public void Explode ()
    {
        AudioManager.Play("explosion");
        ParticleManager.Play("BigExplosion", transform.position);
        GameObject deadX = Spawner.Spawn("DeadX", transform.position);
        deadX.transform.forward = transform.forward;
        gameObject.SetActive(false);
        GameManager.TankKilled(this);
    }

    public void BulletDisabled ()
    {
        bulletsLeft++;
    }

    public void MineDisabled ()
    {
        minesLeft++;
    }

    public bool CanHit (Vector3 dir, TankController target)
    {
        dir.Normalize();
        Vector3 origin = transform.position + Vector3.up + dir * 5;
        Ray ray = new Ray(origin, dir);
        RaycastHit hit;
        for (int i = 0; i <= maxBounces; i++)
        {
            if (Physics.SphereCast(ray, 0.5f, out hit, hitMask))
            {
                TankController otherTank = hit.collider.GetComponentInParent<TankController>();
                if (otherTank == target) 
                {
                    Debug.Log("FOUND TARGET TANK");
                    return true;
                }
                else if (otherTank != null || otherTank == this)
                {
                    Debug.Log("FOUND OTHER TANK: ", otherTank);
                    return false;
                }
                else 
                {
                    ray.origin = hit.point + hit.normal*0.5f;
                    ray.direction = ray.direction - 2 * Vector3.Dot(ray.direction, hit.normal) * hit.normal;
                }
            }
        }
        return false;
    }
}
